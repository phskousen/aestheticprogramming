class Headline {
  constructor(_x,_y){
    this.x    = _x;
    this.y    = _y;
  }

  show(headTxt, _x, _y) {
    textFont('Bebas Neue');
    textSize(100);
    textStyle(BOLD);
    textAlign(CENTER);
    fill(255);
    text(headTxt, this.x, this.y);
  }
}

class Button {
  constructor(_n, _x, _y, _w, _h, _r) {
    this.name = _n;
    this.x    = _x;
    this.y    = _y;
    this.w    = _w;
    this.h    = _h;
    this.r    = _r;
    this.isClicked = null;
  }

  show() {
    fill(255);
    rectMode(CENTER);
    rect(this.x, this.y, this.w, this.h, this.r);
    fill(0);
    textAlign(CENTER, CENTER);
    textSize(24);
  }

  say(txt) {
    text(txt, this.x, this.y);
  }

  clicked() {
    //define borders of button
    if(mouseX > this.x-this.w/2 && mouseX < this.x+this.w/2 &&
       mouseY > this.y-this.h/2 && mouseY < this.y+this.h/2){
      print(this.name+' clicked');
      this.isClicked = true;
    } else {
      this.isClicked = false;
    }
  }
}
