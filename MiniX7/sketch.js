let auroraLine = [];

function setup(){
  createCanvas(windowWidth, windowHeight);
  frameRate(60);

  green1 = color(2, 115, 115);
  green2 = color(4, 191, 173);
  green3 = color(44, 191, 157);
  red1 = color(140, 3, 39);
  red2 = color(217, 67, 107);
  red3 = color(242, 119, 164);
  colorsA = [green1,green2,green3];
  colorsB = [red1, red2, red3];
  initX = 5;
  yOff = 0.00;

  for (let i = 0; i < 600; i++){
    yOff += 0.5;
    n = noise(yOff)*500;
    rcA = colorsA[floor(random(0,3))];
    rcB = colorsB[floor(random(0,3))];
    rl = random(40,400);
    auroraLine[i] = new Aurora(initX+i*5, n, rl, 1, rcB);
  }
}

function draw(){
  background (23, 32, 38, 20);

  for (let i = 0; i < auroraLine.length; i++){
    auroraLine[i].show();
    auroraLine[i].move();
  }
}
