# MiniX 2 - Variable geometry

## Random emoji generator

<img src="https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/MiniX2/media/Skærmbillede%202022-03-17%20kl.%2012.31.36.png" width="50%" height="50%" align=""/>
<img src="https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/MiniX2/media/Skærmbillede%202022-03-17%20kl.%2012.31.47.png" width="50%" height="50%" align="right" />
<img src="https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/MiniX2/media/Skærmbillede%202022-03-17%20kl.%2012.31.54.png" width="50%" height="50%" align=""/>
<img src="https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/MiniX2/media/Sk%C3%A6rmbillede_2022-04-01_kl._15.17.59.png" width="50%" height="50%" align="right"/>

#### Describe your program and what you have used and learnt.
For this week's MiniX, I have created a program which, when the left mouse button is clicked, generates an emoji with semi-random eye shapes and mouth curvature. 
For the emoji face, two shapes are used; Ellipses and a curved line, forming the eyes and mouth respectively, are called with the p5.js functions `arc()` and `curve()`. Here, the `arc()` function is called rather the `ellipse()` function, as this allows me to have greater control over the shape of the eyes by defining a starting and ending point of the circle using the radians. Likewise, using the `arc()` function rather than `line()` allows me to create a line with a variable curvature. This was essential for the appearance of the mouth, as it allows for greater expression and variety in moods.
The generation of a variety of different shapes is controlled by the built-in p5 function `mouseClicked()`, which listens for a mouse click event. Every time the mouse is clicked, the program assigns a pseudo-random radians value to the eyes. This value is chosen from an array consisting of three values: 

    function mouseClicked(){
      eL_ran = random([PI,PI+QUARTER_PI,TWO_PI]);
      eR_ran = random([PI,PI+QUARTER_PI,TWO_PI]);
     [...]
    }


#### How would you put your emoji into a wider social and cultural context that concerns a politics of representation, identity, race, colonialism, and so on? (Try to think through the assigned reading and your coding process, and then expand that to your own experience and thoughts - this is a difficult task, you may need to spend some time thinking about it).
Through the age of digital media, emoji culture has been steadily growing and evolving; this is especially true in the age of smartphones, where communication by text has become faster, more efficient and omnipresent in many parts of the world. As a result, emojis have become a way to quickly and easily express emotions, thoughts and concepts through use of small, often easy to decipher images. Along with an increase of emoji usage, the politics of representation have become central to academic explorations of the phenomenon.  

In contrast to the conventional usage of emojis through which one is able to decide which emoji to use and thus express their own feelings, thoughts, ideas, etc., my program instead leaves this decision to the computer. One way of interpretation in regards to this is that by refusing the human actor to pick and choose from a predefined range of emotions and/or expressions, the computer becomes the agent which imposes an arbitrary feeling onto the user, rather than vice versa. Another possible interpretation is that the program in some way "facializes" the computer itself, giving the illusion of an emotionally animated digital being.

For the source code, [click here](https://gitlab.com/phskousen/aestheticprogramming/-/tree/main/MiniX2).<br>
For and executable version of the program, please [click here](https://phskousen.gitlab.io/aestheticprogramming/MiniX2). Click the screen anywhere to generate an emoji.


