let mx, my;
let bg;
let ts = 10;

function setup(){
  createCanvas(windowWidth, windowHeight);
  bg = createGraphics(windowWidth, windowHeight);
  frameRate(60);
  cursor(CROSS);
  captureData();
}

function draw() {
  background(0,100,100);
  captureCursor();
  image(bg,0,0,width,height);
}

function captureData(){
  //Base Javascript syntax which returns operating system. Inspired by
  //https://dev.to/vaibhavkhulbe/get-os-details-from-the-webpage-in-javascript-b07
  let os = null;
  if (navigator.appVersion.indexOf("Mac") != -1) {
    os = "macos";
  } else if (navigator.appVersion.indexOf("Win") != -1) {
    os = "windows";
  } else if (navigator.appVersion.indexOf("Linux") != -1) {
    os = "linux";
  } else if (navigator.appVersion.indexOf("iOS") != -1) {
    os="ios";
  }
  bg.fill(0,50);
  bg.textSize(ts);
  bg.text("they're the type to use "+os, 20,20);

  //Battery status API borrowed from the Mozilla developer resources @
  //https://developer.mozilla.org/en-US/docs/Web/API/Battery_Status_API
  navigator.getBattery()
    .then(function (battery) {
    let bl = battery.level;
    bg.fill(0,50);
    bg.textSize(ts);
    bg.text("their battery charge is "+bl*100+"%", 20,40);
  });
}

function captureCursor(){
  mx = mouseX;
  my = mouseY;
  push();
  stroke(0,50);
  line(0,my,width,my);
  line(mx,0,mx,height);
  pop();
  fill(0,50);
  textSize(ts);

  if (mx <= 0 || mx >= width || my <= 0 || my >= height) {
    text("they moved their cursor outside the window", mx+15, my+25);
  } else {
    text("their cursor position is "+mx+" , "+my, mx+15,my+25);
  }
  if (mouseIsPressed == true) {
    text("they're pressing the mouse", mx+15, my-15);
  }
}

function mouseReleased(){
  bg.fill(0,50);
  bg.textSize(ts);
  bg.text("they clicked at "+mx+" , "+my, mx, my);
}

function keyPressed(){
  let metaText = ["they just typed ", "they wrote ", "they pressed "];
  bg.fill(0,50);
  bg.textSize(10);
  bg.text(random(metaText) + key, mouseX, mouseY);
}
