# MiniX 9 : Flowcharts

![Individual flowchart of an existing program](https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/MiniX9/media/MiniX9%20individual.jpg)

<img src="https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/MiniX9/media/Sk%C3%A6rmbillede_2022-04-24_120800.png" alt="Group flowchart idea 1" style="height: 50px;"/>
<img src="https://gitlab.com/phskousen/aestheticprogramming/-/raw/main/MiniX9/media/Sk%C3%A6rmbillede_2022-04-24_120825.png" alt="Group flowchart idea 2" style="height: 50%;"/>

While the general concept of a flowchart remains the same across the different charts produced for this MiniX  - that is, they visualize algorithmic processes in a manageble and coherent manner, while translating technical programming concepts into natural language - their purposes, and the ways by which they came to be, are varying. The main difference here would be the point in time they were made in relation to the overall process. 

For the group work, the flowcharts were created before the program itself. In this sense, the flowcharts were useful as a means of structuring, communicating, and a visualizing our ideas to each other. Further, it proved useful as a sort road-map to follow in the programming phase of the creation of the program.

In the case of the individual program, the flowchart serves primarily as a communications tool, which communicates the internal processes of the program. By translating indivudal processes into plain english, the flowchart is able to represent the inner workings of the program, making it understandable for someone less-versed in the field of computer coding and programming language. 

Common to the production of all three flowcharts is the challenge of deciding what information to include, and what to exclude. Picking and choosing various elements is a fine line between oversimplifying the program and making it illegible, ultimately neglecting the whole point of the flowchart, which is to clearly communicate processes. When developing and thinking about how we should go about forming the flowcharts, it quickly became apparent that expressing coding concepts in regular english proved a greater challenge than firstly anticipated. Furthermore, the act of creating a flowchart of a non-existing program was difficult, as we were constantly aware of our own programming experience and skills, which oftentimes led the process to a halt.

Making flowcharts _before_ coding the program has proven to be a difficult task. One has to be aware of ones own programming skills; on the flip-side, if you are able to see past the restrictions of your own skills, creating the flowchart first can allow for more freedom and act as another tool for idea generation, as well as a great tool for communication, brainstorming, and planning.
