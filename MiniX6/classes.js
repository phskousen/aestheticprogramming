class Character {
  constructor(xPos, yPos, _r) {
    this.x = xPos;
    this.y = yPos;
    this.r = _r
    this.speedMod = 1;
  }

  show(){
    noStroke();
    fill(255);
    let conX = constrain(this.x, 0, width);
    let conY = constrain(this.y, 0, height);
    ellipse(conX, conY, this.r);
  }
}

class Obstacle {
  constructor(_x, _y, _w, _h) {
    this.speed = 3;
    this.x = _x;
    this.y = _y;
    this.w = _w;
    this.h = _h;
  }
  move() {
    this.y += this.speed;
  }
  show() {
    push();
    noStroke();
    fill(0);
    rect(this.x, this.y, this.w, this.h);
    pop();
 }
}

class Counter {
  constructor(_x, _y, size){
    this.x = _x;
    this.y = _y;
    this.size = size;
    this.points = 0;
    this.counterText;
  }

  show(){
    push();
    fill(255);
    textAlign(CENTER);
    textSize(this.size);
    text(this.counterText, this.x, this.y);
    pop();
  }
}
